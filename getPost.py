import os
import sqlite3
import datetime

def getTitle():
    challengeName = input("Enter Challenge Name: ")
    CTFName = input("Enter CTF Name: ")
    year = datetime.date.today().year
    title = challengeName + " - " + CTFName + " " + str(year)
    return title, CTFName

def getCategory():
    allcategories = ['Crypto', 'Forensics', 'Hardware', 'Misc', 'Pentest', 'Pwn', 'RE', 'Web']
    
    print('Please Choose the writeup category from the below list.\n')
    
    for i in range(len(allcategories)):
        print(f'{i}. {allcategories[i]}')
    
    choice = int(input('Enter your choice: '))

    return allcategories[choice]

def getAuthor(db):
    dbFile = str(db)
    dbFile = sqlite3.connect(dbFile)
    cursor = dbFile.cursor()

    cursor.execute(
    '''
        SELECT Name, url FROM authors;
    ''')

    allauthors = cursor.fetchall()

    author = input('Enter the writeup author name: ')
    if len(allauthors) > 0:
        for i in allauthors:
            if author == i[0]:
                authorURL = i[1]
                
    # Still in progress
    if(authorURL is None):
        print('Author Name not found in database\nDo you want to add author details to database?(Y/N)')
        choice = input()
        if str(choice) == 'Y':
            authorURL = input(f"Enter {author}'s Twitter profile link: ")
            cursor.execute(f'''INSERT INTO authors (Name, url) VALUES ({author}, {authorURL})''')
        else:
            print('Please fill your twitter profile after the writeup README is created')
            authorURL = ''

    return author, authorURL

def getDate():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def createREADME(title, category, date, author, authorURL, tags):
    template = f'''---\ntitle: {title}\ndate: {date}\nauthor: {author}\nauthor_url: {authorURL}\ncategories:\n  - {category}\ntags:\n  - {tags}\n---\n\n**tl;dr**\n\n+ \n+ \n\n<!--more-->\n\n**Challenge Points**: \n**No. of solves**: \n**Solved by**: [{author}]({authorURL})\n'''

    title = title.replace(' ', '')
    f = open(f'source/_posts/{category}/{title}.md','w')
    f.write(template)
    f.close()

    print(f'\nWriteup README has been created\nCheck it out at: source/_posts/{category}/{title}.md')

def main():
    title, CTFName = getTitle()
    category = getCategory()
    author, authorURL = getAuthor('blog.db')
    date = getDate()
    tags = CTFName.replace(' ', '')
    
    createREADME(title, category, date, author, authorURL, tags)

if __name__ == '__main__':
    main()